package ipl_project;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.Collections;

class Main {
    public static final int MATCH_ID = 0;
    public static final int INNING = 1;
    public static final int BATTING_TEAM = 2;
    public static final int BOWLING_TEAM = 3;
    public static final int OVER = 4;
    public static final int BALL = 5;
    public static final int BATSMAN = 6;
    public static final int NON_STRIKER = 7;
    public static final int BOWLER = 8;
    public static final int IS_SUPER_OVER = 9;
    public static final int WIDE_RUNS = 10;
    public static final int BYE_RUNS = 11;
    public static final int LEGBYE_RUNS = 12;
    public static final int NOBALL_RUNS = 13;
    public static final int PENALTY_RUNS = 14;
    public static final int BATSMAN_RUNS = 15;
    public static final int EXTRA_RUNS = 16;
    public static final int TOTAL_RUNS = 17;
    public static final int PLAYER_DISMISSED = 18;
    public static final int DISMISSAL_KIND = 19;
    public static final int FIELDER = 20;
    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int CITY = 2;
    public static final int DATE = 3;
    public static final int TEAM1 = 4;
    public static final int TEAM2 = 5;
    public static final int TOSS_WINNER = 6;
    public static final int TOSS_DECISION = 7;
    public static final int RESULT = 8;
    public static final int DL_APPLIED = 9;
    public static final int WINNER = 10;
    public static final int WIN_BY_RUN = 11;
    public static final int WIN_BY_WICKET = 12;
    public static final int PLAYER_OF_MATCH = 13;
    public static final int VENUE = 14;
    public static final int UMPIRE1 = 15;
    public static final int UMPIRE2 = 16;
    public static final int UMPIRE3 = 17;

    public static void main(String args[]) {
        List<Delivery> deliveries = getDeliveries();
        List<Match> matches = getMatches();

        findCountOfMatchPerYear(matches);
        findCountOfMatchWonPerTeam(matches);
        findExtraRunsConcededPerTeamIn2016(matches, deliveries);
        findTopEcoBowlerIn2015(matches, deliveries);
        findManOfTheMatchCountPerPlayer(matches);
        findWicketsByBowlerWithVenueIn2016(matches, deliveries);
    }

    private static void findWicketsByBowlerWithVenueIn2016(List<Match> matches, List<Delivery> deliveries) {
        Map<String, String> venueWithMatchIdIn2016 = new TreeMap<>();
        int year = 2016;

        for (Match match : matches) {
            String matchId = match.getId();
            String matchVenue = match.getVenue();
            int season = Integer.parseInt(match.getSeason());
            if (season == year) {
                if (!venueWithMatchIdIn2016.containsKey(matchId)) {
                    venueWithMatchIdIn2016.put(matchId, matchVenue);
                }
            }
        }
        Map<String, Map<String, Integer>> wicketByBowlerWithVenue = new TreeMap<>();

        for (Delivery delivery : deliveries) {
            String deliveryMatchId = delivery.getMatch_id();
            String playerDismissed = delivery.getPlayer_dismissed();
            String bowler = delivery.getBowler();
            String venue = venueWithMatchIdIn2016.get(deliveryMatchId);
            if (venueWithMatchIdIn2016.containsKey(deliveryMatchId)) {
                if (!wicketByBowlerWithVenue.containsKey(venue)) {
                    wicketByBowlerWithVenue.put(venue, new TreeMap<String, Integer>());
                    if (!playerDismissed.isEmpty()) {
                        wicketByBowlerWithVenue.get(venue).put(bowler, 1);
                    }
                } else {
                    if (!playerDismissed.isEmpty()) {
                        if (!wicketByBowlerWithVenue.get(venue).containsKey(bowler)) {
                            wicketByBowlerWithVenue.get(venue).put(bowler, 1);
                        } else {
                            wicketByBowlerWithVenue.get(venue).put(bowler, wicketByBowlerWithVenue.get(venue).get(bowler) + 1);
                        }
                    }
                }
            }
        }
        System.out.println("Wickets taken by each player venue wise in 2016 :");
        System.out.println(wicketByBowlerWithVenue + "\n");
    }

    static void findManOfTheMatchCountPerPlayer(List<Match> matches) {
        Map<String, Integer> manOfTheMatchCountPerPlayer = new TreeMap<>();
        for (Match match : matches) {
            String player = match.getPlayer_of_match();
            if (!player.isEmpty()) {
                if (!manOfTheMatchCountPerPlayer.containsKey(player)) {
                    manOfTheMatchCountPerPlayer.put(player, 1);
                } else {
                    manOfTheMatchCountPerPlayer.put(player, manOfTheMatchCountPerPlayer.get(player) + 1);
                }
            }
        }
        System.out.println("Players who won man of the match and number of times they won award in IPL. :");
        System.out.println(manOfTheMatchCountPerPlayer + "\n");
    }

    static void findTopEcoBowlerIn2015(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Integer> matchIdIn2015 = new TreeMap<>();
        int year = 2015;
        for (Match match : matches) {
            String matchId = match.getId();
            int season = Integer.parseInt(match.getSeason());
            if (season == year) {
                if (!matchIdIn2015.containsKey(matchId)) {
                    matchIdIn2015.put(matchId, 1);
                }
            }
        }
        Map<String, ArrayList<Integer>> ballsAndRunsPerBowler = new TreeMap<String, ArrayList<Integer>>();
        for (Delivery delivery : deliveries) {
            String matchId = delivery.getMatch_id();
            String bowler = delivery.getBowler();
            int balls = Integer.parseInt(delivery.getBall());
            int runs = Integer.parseInt(delivery.getTotal_runs());
            if (matchIdIn2015.containsKey(matchId)) {
                if (!ballsAndRunsPerBowler.containsKey(bowler)) {
                    ballsAndRunsPerBowler.put(bowler, new ArrayList<Integer>());
                    ballsAndRunsPerBowler.get(bowler).add(1);
                    ballsAndRunsPerBowler.get(bowler).add(runs);
                } else {
                    List<Integer> playerBalls = ballsAndRunsPerBowler.get(bowler);
                    Integer ballsCount = playerBalls.get(0);
                    Integer runsCount = playerBalls.get(1);
                    playerBalls.set(0, ballsCount + 1);
                    playerBalls.set(1, runsCount + runs);
                }
            }
        }
        Map<Double, String> bowlersEcoRate = new TreeMap<>();
        for (String bowler : ballsAndRunsPerBowler.keySet()) {
            double avg = (ballsAndRunsPerBowler.get(bowler).get(1)) / ((ballsAndRunsPerBowler.get(bowler).get(0)) / 6.0);
            bowlersEcoRate.put(avg, bowler);
        }
        double min = Collections.min(bowlersEcoRate.keySet());
        System.out.println("Top Economical Bowler for the year 2015 is : " + bowlersEcoRate.get(min) + "\n");
    }

    static void findExtraRunsConcededPerTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Integer> matchIdIn2016 = new TreeMap<>();
        int year = 2016;
        for (Match match : matches) {
            String matchId = match.getId();
            int season = Integer.parseInt(match.getSeason());
            if (season == year) {
                if (!matchIdIn2016.containsKey(matchId)) {
                    matchIdIn2016.put(matchId, 1);
                }
            }
        }
        Map<String, Integer> extraRunsPerTeam = new TreeMap<>();
        for (Delivery delivery : deliveries) {
            String matchId = delivery.getMatch_id();
            String battingTeam = delivery.getBatting_team();
            int extraRuns = Integer.parseInt(delivery.getExtra_runs());
            if (matchIdIn2016.containsKey(matchId)) {
                if (!extraRunsPerTeam.containsKey(battingTeam)) {
                    extraRunsPerTeam.put(battingTeam, extraRuns);
                } else {
                    extraRunsPerTeam.put(battingTeam, extraRunsPerTeam.get(battingTeam) + extraRuns);
                }
            }
        }
        System.out.println("Extra runs conceded per team for the year 2016 :");
        System.out.println(extraRunsPerTeam + "\n");
    }

    static void findCountOfMatchWonPerTeam(List<Match> matches) {
        Map<String, Integer> noOfMatchWonPerTeam = new TreeMap<>();
        for (Match match : matches) {
            String team1 = match.getTeam1();
            String team2 = match.getTeam2();
            String winner = match.getWinner();
            if (!noOfMatchWonPerTeam.containsKey(team1)) {
                noOfMatchWonPerTeam.put(team1, 0);
            }
            if (!noOfMatchWonPerTeam.containsKey(team2)) {
                noOfMatchWonPerTeam.put(team2, 0);
            }
            if (!winner.isEmpty()) {
                noOfMatchWonPerTeam.put(winner, noOfMatchWonPerTeam.get(winner) + 1);
            }
        }
        System.out.println("Number of match won per team over all year in IPL :");
        System.out.println(noOfMatchWonPerTeam + "\n");
    }

    static void findCountOfMatchPerYear(List<Match> matches) {
        Map<String, Integer> matchCountPerYear = new TreeMap<>();
        for (Match match : matches) {
            String year = match.getSeason();
            if (matchCountPerYear.containsKey(year)) {
                matchCountPerYear.put(year, matchCountPerYear.get(year) + 1);
            } else {
                matchCountPerYear.put(year, 1);
            }
        }
        System.out.print("Count of IPL Match each Year : ");
        System.out.println(matchCountPerYear + "\n");
    }

    static List<Delivery> getDeliveries() {
        List<Delivery> deliveries = new ArrayList<Delivery>();
        boolean isFirstLine = true;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Resources/deliveries.csv"));
            Delivery delivery;
            String line = "";
            while ((line = reader.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue;
                }
                String[] record = line.split(",", -1);
                delivery = new Delivery();
                delivery.setMatch_id(record[MATCH_ID]);
                delivery.setInning(record[INNING]);
                delivery.setBatting_team(record[BATTING_TEAM]);
                delivery.setBowling_team(record[BOWLING_TEAM]);
                delivery.setOver(record[OVER]);
                delivery.setBall(record[BALL]);
                delivery.setBatsman(record[BATSMAN]);
                delivery.setNon_striker(record[NON_STRIKER]);
                delivery.setBowler(record[BOWLER]);
                delivery.setIs_super_over(record[IS_SUPER_OVER]);
                delivery.setWide_runs(record[WIDE_RUNS]);
                delivery.setBye_runs(record[BYE_RUNS]);
                delivery.setLegbye_runs(record[LEGBYE_RUNS]);
                delivery.setNoball_runs(record[NOBALL_RUNS]);
                delivery.setPenalty_runs(record[PENALTY_RUNS]);
                delivery.setBatsman_runs(record[BATSMAN_RUNS]);
                delivery.setExtra_runs(record[EXTRA_RUNS]);
                delivery.setTotal_runs(record[TOTAL_RUNS]);
                delivery.setPlayer_dismissed(record[PLAYER_DISMISSED]);
                delivery.setDismissal_kind(record[DISMISSAL_KIND]);
                delivery.setFielder(record[FIELDER]);
                deliveries.add(delivery);
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("File Not Found");
        }
        return deliveries;
    }

    static List<Match> getMatches() {
        List<Match> matches = new ArrayList<Match>();
        boolean isFirstLine = true;
        try {
            BufferedReader reader = new BufferedReader(new FileReader("Resources/matches.csv"));
            Match match;
            String line = "";
            while ((line = reader.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue;
                }
                String[] record = line.split(",", -1);
                match = new Match();
                match.setId(record[ID]);
                match.setSeason(record[SEASON]);
                match.setCity(record[CITY]);
                match.setDate(record[DATE]);
                match.setTeam1(record[TEAM1]);
                match.setTeam2(record[TEAM2]);
                match.setToss_winner(record[TOSS_WINNER]);
                match.setToss_decision(record[TOSS_DECISION]);
                match.setResult(record[RESULT]);
                match.setDl_applied(record[DL_APPLIED]);
                match.setWinner(record[WINNER]);
                match.setWin_by_runs(record[WIN_BY_RUN]);
                match.setWin_by_wickets(record[WIN_BY_WICKET]);
                match.setPlayer_of_match(record[PLAYER_OF_MATCH]);
                match.setVenue(record[VENUE]);
                match.setUmpire1(record[UMPIRE1]);
                match.setUmpire2(record[UMPIRE2]);
                match.setUmpire3(record[UMPIRE3]);
                matches.add(match);
            }
            reader.close();
        } catch (IOException e) {
            System.out.println("File Not Found");
        }
        return matches;
    }
}
